# Bienvenido a Macros Python

Desarrollar macros en [LibreOffice][1] con [Python][2].

### Contenido:

1. [Instroducción](introduction.md)
1. Macros
1. Funciones de Calc
1. Extensiones
1. Nuevos componentes

[1]: https://libreoffice.org
[2]: https://python.org


