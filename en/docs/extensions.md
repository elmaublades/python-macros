[//]: # (Copyright (c)  2018  Mauricio Baeza web [AT] correlibre [DOT] net)
[//]: # (Permission is granted to copy, distribute and/or modify this document)
[//]: # (under the terms of the GNU Free Documentation License, Version 1.3)
[//]: # (or any later version published by the Free Software Foundation;)
[//]: # (with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.)
[//]: # (A copy of the license is included in the section entitled "GNU)
[//]: # (Free Documentation License".)


## Extensions

#### Our first minimal extension


In this chapter, we'll make our first Add-on (extension) for LibreOffice.

* We create a folder for work.

        mkdir hello_world
        cd hello_world

<br>

* First, we write the code python of our extension in the file: **hello.py**

        # coding: utf-8

        import unohelper
        from com.sun.star.task import XJobExecutor


        ID = 'org.universolibre.test.hello'


        class HelloWorld(unohelper.Base, XJobExecutor):

            def __init__(self, ctx):
                self.ctx = ctx

            def msgbox(self, message):
                service_manager = self.ctx.getServiceManager()
                toolkit = service_manager.createInstance('com.sun.star.awt.Toolkit')
                parent = toolkit.getDesktopWindow()
                message_box = toolkit.createMessageBox(parent, 0, 1, 'Python Macros', message)
                return message_box.execute()

            def trigger(self, *args):
                self.msgbox('Hello World...')
                return


        g_ImplementationHelper = unohelper.ImplementationHelper()
        g_ImplementationHelper.addImplementation(HelloWorld, ID, (ID,))

* Note that, the function msgbox it's the same that we used in the
[macros chapter](macros/#show-information).
* Method **trigger** is inherit from class [XJobExecutor][1], this is the point
of access for  the called of extension.

<br>

* Now, we make the file: **Addons.xcu**, in this file, we describe la structure
of menu for our extension.

        <?xml version='1.0' encoding='UTF-8'?>
        <oor:component-data xmlns:oor="http://openoffice.org/2001/registry" xmlns:xs="http://www.w3.org/2001/XMLSchema" oor:name="Addons" oor:package="org.openoffice.Office">
            <node oor:name="AddonUI">
                <node oor:name="OfficeMenuBar">
                    <node oor:name="Hello_World_Test.OfficeMenuBar" oor:op="replace">
                        <prop oor:name="Context" oor:type="xs:string">
                            <value>com.sun.star.sheet.SpreadsheetDocument,com.sun.star.text.TextDocument,</value>
                        </prop>
                        <prop oor:name="Title" oor:type="xs:string">
                            <value>My extension</value>
                        </prop>
                        <node oor:name="Submenu">
                            <node oor:name="m0" oor:op="replace">
                                <prop oor:name="Context" oor:type="xs:string">
                                    <value>com.sun.star.sheet.SpreadsheetDocument,com.sun.star.text.TextDocument,</value>
                                </prop>
                                <prop oor:name="Title" oor:type="xs:string">
                                    <value>Say Hello</value>
                                </prop>
                                <prop oor:name="URL" oor:type="xs:string">
                                    <value>service:org.universolibre.test.hello?say</value>
                                </prop>
                                <prop oor:name="Target" oor:type="xs:string">
                                    <value>_self</value>
                                </prop>
                            </node>
                        </node>
                    </node>
                </node>
            </node>
        </oor:component-data>

<br>

* Our next file: **description.xml**, is useful for show information to user
when install it. More later we see all options for this file.

        <?xml version="1.0" encoding="UTF-8"?>
        <description xmlns="http://openoffice.org/extensions/description/2006"
            xmlns:xlink="http://www.w3.org/1999/xlink"
            xmlns:d="http://openoffice.org/extensions/description/2006">
            <identifier value="org.universolibre.test.hello" />
            <display-name>
                <name lang="en">My great extension Hello World</name>
            </display-name>
        </description>

<br>

* For last, we create the folder: META-INF and inside it, the file:
**manifest.xml**, for describe the list of files of our Add-on.

        <?xml version="1.0" encoding="UTF-8"?>
        <manifest:manifest>
         <manifest:file-entry manifest:full-path="hello.py" manifest:media-type="application/vnd.sun.star.uno-component;type=Python"/>
         <manifest:file-entry manifest:full-path="Addons.xcu" manifest:media-type="application/vnd.sun.star.configuration-data"/>
        </manifest:manifest>

<br>

* All content, have see it, like:

        hello_world
        ├── Addons.xcu
        ├── description.xml
        ├── hello.py
        └── META-INF
            └── manifest.xml

<br>

* Now, we compress our Add-on. Important: you need compress the **content** of
folder.

    ![Compress files](img/04/img_ext_001.png)

* It's important used format ZIP.

    ![Compress files](img/04/img_ext_002.png)

* Finally, rename file to OXT.

    ![OXT](img/04/img_ext_003.png)

* Ready!, we try our great new Add-on. Open the file OXT with LibreOffice,
must offer you to install it.

    ![Install OXT](img/04/img_ext_004.png)

* Add-on installed.

    ![Installed OXT](img/04/img_ext_005.png)

* Restart LibreOffice, open Calc used the new menu: **My extension -> Say hello**

    ![Menu](img/04/img_ext_006.png)

    ![OK](img/04/img_ext_007.png)

* Happy develop.


[1]: https://api.libreoffice.org/docs/idl/ref/interfacecom_1_1sun_1_1star_1_1task_1_1XJobExecutor.html
