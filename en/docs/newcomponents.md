[//]: # (Copyright (c)  2018  Mauricio Baeza web [AT] correlibre [DOT] net)
[//]: # (Permission is granted to copy, distribute and/or modify this document)
[//]: # (under the terms of the GNU Free Documentation License, Version 1.3)
[//]: # (or any later version published by the Free Software Foundation;)
[//]: # (with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.)
[//]: # (A copy of the license is included in the section entitled "GNU)
[//]: # (Free Documentation License".)


## New components.

In this chapter, we'll make a new UNO component, with this, we can add new
functions to LibreOffice and used it from Python macros or other languages.

It's necessary have install [LibreOffice SDK](introduction/#libreoffice-sdk)

Find the following files: **idlc** and **regmerge**, and folder `idl/libreoffice`

* For ArchLinux, Ubuntu
    * `/usr/lib/libreoffice/sdk/bin/idlc`
    * `/usr/lib/libreoffice/program/regmerge`
    * `/usr/share/idl/libreoffice`


<br>
#### Our first minimal UNO component.

* We create a folder working with the name of our new UNO component.

        mkdir UnoHelloWorld
        cd UnoHelloWorld
        mkdir source

<BR>
<div class="alert-box notice"><span>TIP: </span>
    All UNO components, inherit of XInterface
</div>
<BR>

* We create the file with the specify type for our new UNO component, for this,
we use the meta language UNOIDL (UNO Interface Definition Language):
**XUnoHelloWorld.idl**

        #include <com/sun/star/uno/XInterface.idl>

        module org { module universolibre { module UnoHelloWorld {

            interface XMyTools : com::sun::star::uno::XInterface
            {
                void sayhello([in] any message);
            };

            service UnoHelloWorld {
                interface XMyTools;
            };

        }; }; };

<br>

* We're going to compile file **IDL** for to produce a binary type library file
**URD** by executing next command.

        /usr/lib/libreoffice/sdk/bin/idlc -I /usr/share/idl/libreoffice XUnoHelloWorld.idl
        Compiling: XUnoHelloWorld.idl

<br>

* The result is the new file **URD**.

        UnoHelloWorld
        ├── source
        ├── XUnoHelloWorld.idl
        └── XUnoHelloWorld.urd

<br>

* Now, we compile and merged file **URD** into a file registry **RDB** by
executing next command.

        /usr/lib/libreoffice/program/regmerge XUnoHelloWorld.rdb /UCR XUnoHelloWorld.urd

<br>

* The result is the new file **RDB**. Move this file into folder **source**.

        UnoHelloWorld
        ├── source
        │   └── XUnoHelloWorld.rdb
        ├── XUnoHelloWorld.idl
        └── XUnoHelloWorld.urd

<br>

* All the next files, are create inside the folder **source**. First, we create
file: **UnoHelloWorld.py**, with the source code for the new component.

        # coding: utf-8

        import unohelper
        from org.universolibre.UnoHelloWorld import XMyTools


        ID_EXT = 'org.universolibre.UnoHelloWorld'
        SRV_JOB = ('com.sun.star.task.Job',)


        class UnoHelloWorld(unohelper.Base, XMyTools):

            def __init__(self, ctx):
                self.ctx = ctx
                self.sm = self.ctx.getServiceManager()
                self.toolkit = self.sm.createInstance('com.sun.star.awt.Toolkit')

            def msgbox(self, message):
                parent = self.toolkit.getDesktopWindow()
                message_box = self.toolkit.createMessageBox(
                    parent, 0, 1, 'Python Macros', message)
                return message_box.execute()

            def sayhello(self, message):
                msg = 'Hello {}'.format(message or 'World')
                self.msgbox(msg)
                return


        g_ImplementationHelper = unohelper.ImplementationHelper()
        g_ImplementationHelper.addImplementation(UnoHelloWorld, ID_EXT, SRV_JOB)

<br>

* The next file: **description.xml**, it's used for show information of function,
when installed it.

        <?xml version="1.0" encoding="UTF-8"?>
        <description
            xmlns="http://openoffice.org/extensions/description/2006"
            xmlns:d="http://openoffice.org/extensions/description/2006"
            xmlns:xlink="http://www.w3.org/1999/xlink">

            <identifier value="org.universolibre.UnoHelloWorld" />

            <display-name>
                <name lang="en">My great UNO Component</name>
            </display-name>

        </description>

<br>

* For last, we create the folder: **META-INF** and inside it, the file:
**manifest.xml**, for describe the list of files of our component.

        <?xml version="1.0" encoding="UTF-8"?>
        <manifest:manifest>
            <manifest:file-entry manifest:full-path="UnoHelloWorld.py" manifest:media-type="application/vnd.sun.star.uno-component;type=Python"/>
            <manifest:file-entry manifest:full-path="XUnoHelloWorld.rdb" manifest:media-type="application/vnd.sun.star.uno-typelibrary;type=RDB"/>
        </manifest:manifest>

<br>

* All content, have see it, like:

        UnoHelloWorld
        ├── source
        |   ├── META-INF
        │   │   └── manifest.xml
        │   ├── description.xml
        │   ├── UnoHelloWorld.py
        │   └── XUnoHelloWorld.rdb
        ├── XUnoHelloWorld.idl
        └── XUnoHelloWorld.urd

<br>

* Now, we compress our Add-in. **Important**: you need compress the **content**
of folder **source**.

![Compress files](img/05/img_component_01.png)

* It's important used format **ZIP**.

![Compress files](img/05/img_component_02.png)

* Finally, rename file to **OXT**, and move file outside of folder **source**.

![OXT](img/05/img_component_03.png)

* Ready!, we try our great new UNO component. Open the file **OXT** with
LibreOffice, must offer you to install it.

![Install OXT](img/05/img_component_04.png)

* Extension installed.

![Install OXT](img/05/img_component_05.png)

* Test from Python

        #!/usr/bin/env python3

        import uno


        def test_myuno():
            app = create_service('org.universolibre.UnoHelloWorld', True)
            app.sayhello('')
            app.sayhello('Python')
            return


        def create_service(name, with_context):
            context = uno.getComponentContext()
            service_manager = context.getServiceManager()
            if with_context:
                service = service_manager.createInstanceWithContext(name, context)
            else:
                service = service_manager.createInstance(name)
            return service

<br>

* Test from Basic

        Sub TestMyUNO()
            app = createUnoService("org.universolibre.UnoHelloWorld")
            app.sayhello("")
            app.sayhello("Python")
        End Sub

<br>

* Happy develop.
