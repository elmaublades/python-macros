[//]: # (Copyright (c)  2018  Mauricio Baeza web [AT] correlibre [DOT] net)
[//]: # (Permission is granted to copy, distribute and/or modify this document)
[//]: # (under the terms of the GNU Free Documentation License, Version 1.3)
[//]: # (or any later version published by the Free Software Foundation;)
[//]: # (with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.)
[//]: # (A copy of the license is included in the section entitled "GNU)
[//]: # (Free Documentation License".)


<div style="text-align: right">
An expert is a man who has made <br>
all the mistakes which can be made, <br>
in a narrow field.
<BR>
<b>Niels Bohr</b>
</div>


## Calc functions

In this chapter, we'll make our first Calc Add-in, with this, we can add new
functions inside Calc like as the built-in functions and used in cell formulas.
A add-in is a UNO component.

It's necessary have install [LibreOffice SDK](introduction/#libreoffice-sdk)

Find the following files: **idlc** and **regmerge**, and folder `idl/libreoffice`

* For ArchLinux, Ubuntu
    * `/usr/lib/libreoffice/sdk/bin/idlc`
    * `/usr/lib/libreoffice/program/regmerge`
    * `/usr/share/idl/libreoffice`


<br>
#### Our first minimal function

* We create a folder working with the name of our new function

        mkdir HelloWorld
        cd HelloWorld
        mkdir source

<BR>
<div class="alert-box notice"><span>TIP: </span>
    All UNO components, inherit of XInterface
</div>
<BR>

* We create the file with the specify type for our new UNO component, for this,
we use the meta language UNOIDL (UNO Interface Definition Language):
**XHelloWorld.idl**


        #include <com/sun/star/uno/XInterface.idl>

        module org { module universolibre { module HelloWorld {

            interface XHelloWorld : com::sun::star::uno::XInterface
            {
                string sayhello([in] any name);
            };

        }; }; };


* In the line:

        string sayhello([in] any name);

* We defined the name of function: **sayhello**, the type to return: **string**,
the argument name: **name**, and your type: **any**, for specify an optional
argument.

        HelloWorld
        ├── source
        └── XHelloWorld.idl

* We're going to compile file **IDL** for to produce a binary type library file
**URD** by executing next command.

        /usr/lib/libreoffice/sdk/bin/idlc -I /usr/share/idl/libreoffice XHelloWorld.idl
        Compiling: XHelloWorld.idl

* The result is the new file **URD**.

        HelloWorld
        ├── source
        ├── XHelloWorld.idl
        └── XHelloWorld.urd

* Now, we compile and merged file **URD** into a file registry **RDB** by
executing next command.

        /usr/lib/libreoffice/program/regmerge XHelloWorld.rdb /UCR XHelloWorld.urd

* The result is the new file **RDB**. Move this file into folder **source**.

        HelloWorld
        ├── source
        │   └── XHelloWorld.rdb
        ├── XHelloWorld.idl
        └── XHelloWorld.urd

* All the next files, are create inside the folder **source**. First, we create
file: **HelloWorld.py**, with the source code for the new function.

        # -*- coding: utf-8 -*-
        import uno
        import unohelper
        from org.universolibre.HelloWorld import XHelloWorld


        ID = 'org.universolibre.HelloWorld'
        SERVICE = ('com.sun.star.sheet.AddIn',)


        class HelloWorld(unohelper.Base, XHelloWorld):

            def __init__(self,ctx):
                self.ctx = ctx

            def sayhello(self, name):
                result = 'Hello {}'.format(name or 'World')
                return result


        def createInstance(ctx):
            return HelloWorld(ctx)


        g_ImplementationHelper = unohelper.ImplementationHelper()
        g_ImplementationHelper.addImplementation(createInstance, ID, SERVICE)

* Now, we create the file: **CalcAddIn.xcu**, this content the information to
show in the functions dialog wizard.

        <?xml version="1.0" encoding="UTF-8"?>
        <oor:component-data xmlns:oor="http://openoffice.org/2001/registry" xmlns:xs="http://www.w3.org/2001/XMLSchema" oor:name="CalcAddIns" oor:package="org.openoffice.Office">
        <node oor:name="AddInInfo">
        <node oor:name="org.universolibre.HelloWorld" oor:op="replace">
        <node oor:name="AddInFunctions">
            <node oor:name="sayhello" oor:op="replace">
            <prop oor:name="DisplayName"><value xml:lang="en">sayhello</value></prop>
            <prop oor:name="Description"><value xml:lang="en">Say hello</value></prop>
            <prop oor:name="Category"><value>Add-In</value></prop>
            <prop oor:name="CompatibilityName"><value xml:lang="en">AutoAddIn.HelloWorld.sayhello</value></prop>
            <node oor:name="Parameters">
            <node oor:name="name" oor:op="replace">
                <prop oor:name="DisplayName"><value xml:lang="en">name</value></prop>
                <prop oor:name="Description"><value xml:lang="en">The name</value></prop>
                </node>
            </node>
            </node>
        </node>
        </node>
        </node>
        </oor:component-data>

* The next file: **description.xml**, it's used for show information of function,
when installed it.

        <?xml version="1.0" encoding="UTF-8"?>
        <description
            xmlns="http://openoffice.org/extensions/description/2006"
            xmlns:d="http://openoffice.org/extensions/description/2006"
            xmlns:xlink="http://www.w3.org/1999/xlink">
            <identifier value="org.universolibre.HelloWorld" />
            <display-name><name lang="en">Say Hello</name></display-name>
        </description>

* For last, we create the folder: **META-INF** and inside it, the file:
**manifest.xml**, for describe the list of files of our Add-in.

        <?xml version="1.0" encoding="UTF-8"?>
        <manifest:manifest>
            <manifest:file-entry manifest:full-path="HelloWorld.py" manifest:media-type="application/vnd.sun.star.uno-component;type=Python"/>
            <manifest:file-entry manifest:full-path="XHelloWorld.rdb" manifest:media-type="application/vnd.sun.star.uno-typelibrary;type=RDB"/>
            <manifest:file-entry manifest:full-path="CalcAddIn.xcu" manifest:media-type="application/vnd.sun.star.configuration-data"/>
        </manifest:manifest>

* All content, have see it, like:

        HelloWorld
        ├── source
        |   ├── META-INF
        │   │   └── manifest.xml
        │   ├── CalcAddIn.xcu
        │   ├── description.xml
        │   ├── HelloWorld.py
        │   └── XHelloWorld.rdb
        ├── XHelloWorld.idl
        └── XHelloWorld.urd

* Now, we compress our Add-in. **Important**: you need compress the **content**
of folder **source**.

![Compress files](img/03/img_function_01.png)

* It's important used format **ZIP**.

![Compress files](img/03/img_function_02.png)

* Finally, rename file to **OXT**, and move file outside of folder **source**.

![OXT](img/03/img_function_03.png)

* Ready!, we try our great new Calc Add-in. Open the file **OXT** with
LibreOffice, must offer you to install it.

![Install OXT](img/03/img_function_04.png)

* Calc Add-in installed.

![Install OXT](img/03/img_function_05.png)

* Restart LibreOffice, open Calc and used Function Wizard.

![Install OXT](img/03/img_function_06.png)

* We can skip argument name, or set directly, or used cell address

![Install OXT](img/03/img_function_07.png)

* Happy develop.
